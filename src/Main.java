import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.print("How many ticket: ");
        Scanner scanner = new Scanner(System.in);
        int tickets = scanner.nextInt();
        int total = 400 * tickets;
        double discount = 0;
        if (tickets > 10) {
            discount = total * 0.1;
        }

        System.out.println("Total: " + total);
        System.out.println("Discount: " + discount);
        System.out.println("Grand Total: " + (total - discount));

    }

}
